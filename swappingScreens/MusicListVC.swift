//
//  MusicListVC.swift
//  swappingScreens
//
//  Created by Nuwan on 2016/10/04.
//  Copyright © 2016 Muse Pixel. All rights reserved.
//

import UIKit

class MusicListVC: UIViewController {
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func rdScreenLoadPressed(_ sender: AnyObject) {
        let songTtitle = "Quit Playing games with my heart"
        
        performSegue(withIdentifier: "SongVC", sender: songTtitle)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.purple
        // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinatin = segue.destination as?
            PlaySongVC{
            if let song = sender as?String{
                destinatin.selectedSong = song
            }
            
        }
    }

  
}
